#pragma once
#include "ClassHeader.h"
#include <fstream>
#include <string>
namespace TestJustCredit {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::IO;

	/// <summary>
	/// ������ ��� MyForm
	/// </summary>

	InputData credit;
	WorkWithBanks banks;
	unsigned int num = 0;

	bool CheckTextBox(String ^_str)
	{
		bool isnumber = true;
		if (_str == "")
		{
			isnumber = false;
		}
		for (int i = 0; i < _str->Length; i++)
		{
			if (isnumber)
			{
				if (_str[i] == '0' || _str[i] == '1' || _str[i] == '2' || _str[i] == '3' || _str[i] == '4' || _str[i] == '5' || _str[i] == '6' || _str[i] == '7' || _str[i] == '8' || _str[i] == '9')
				{
					isnumber = true;
				}
				else
				{
					isnumber = false;
					break;
				}
			}
		}
		return isnumber;
	}

	void WriteHtml(int num)
	{
		wstring str, item;
		wifstream fin;
		fin.open("bd.html");
		getline(fin, str, L'\0');
		fin.close();
		wofstream fout;
		fout.open("index.html");
		str.insert(str.find(L"@"), to_wstring(num + 1));
		str.erase(str.find(L"@"), 1);
		str.insert(str.find(L"@"), to_wstring(banks.CorrectDataCredit.size()));
		str.erase(str.find(L"@"), 1);
		str.insert(str.find(L"@"), banks.CorrectDataCredit[num].NameBank);
		str.erase(str.find(L"@"), 1);
		str.insert(str.find(L"@"), banks.CorrectDataCredit[num].NameCredit);
		str.erase(str.find(L"@"), 1);
		item = banks.Format(to_wstring(banks.CorrectDataCredit[num].Rate));
		str.insert(str.find(L"@"), item);
		str.erase(str.find(L"@"), 1);
		item = banks.Format(to_wstring(banks.CorrectDataCredit[num].Payment));
		str.insert(str.find(L"@"), item);
		str.erase(str.find(L"@"), 1);
		item = banks.Format(to_wstring(banks.CorrectDataCredit[num].Overpayment));
		str.insert(str.find(L"@"), item);
		str.erase(str.find(L"@"), 1);

		fout << str;
		fout.close();
	}
	
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::TextBox^  InputMoney;
	protected:




	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Label^  label4;
	private: System::Windows::Forms::ComboBox^  income;
	private: System::Windows::Forms::ComboBox^  guarantee;



	private: System::Windows::Forms::Label^  label5;

	private: System::Windows::Forms::TextBox^  InputPeriod;
	private: System::Windows::Forms::Button^  calculate;
	private: System::Windows::Forms::Label^  Output;
	private: System::Windows::Forms::WebBrowser^  webBrowser1;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Button^  button2;
	private: System::Windows::Forms::Button^  button3;
	private: System::Windows::Forms::PictureBox^  pictureBox2;
	private: System::Windows::Forms::Button^  button4;
	private: System::Windows::Forms::WebBrowser^  webBrowser2;


	protected:

	private:
		/// <summary>
		/// ������������ ���������� ������������.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// ��������� ����� ��� ��������� ������������ � �� ��������� 
		/// ���������� ����� ������ � ������� ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(MyForm::typeid));
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->InputMoney = (gcnew System::Windows::Forms::TextBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->label4 = (gcnew System::Windows::Forms::Label());
			this->income = (gcnew System::Windows::Forms::ComboBox());
			this->guarantee = (gcnew System::Windows::Forms::ComboBox());
			this->label5 = (gcnew System::Windows::Forms::Label());
			this->InputPeriod = (gcnew System::Windows::Forms::TextBox());
			this->calculate = (gcnew System::Windows::Forms::Button());
			this->Output = (gcnew System::Windows::Forms::Label());
			this->webBrowser1 = (gcnew System::Windows::Forms::WebBrowser());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->button2 = (gcnew System::Windows::Forms::Button());
			this->button3 = (gcnew System::Windows::Forms::Button());
			this->pictureBox2 = (gcnew System::Windows::Forms::PictureBox());
			this->button4 = (gcnew System::Windows::Forms::Button());
			this->webBrowser2 = (gcnew System::Windows::Forms::WebBrowser());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->BeginInit();
			this->SuspendLayout();
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Location = System::Drawing::Point(56, 91);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(180, 13);
			this->label1->TabIndex = 0;
			this->label1->Text = L"������� ����� ������� (� ������)";
			// 
			// InputMoney
			// 
			this->InputMoney->Location = System::Drawing::Point(31, 107);
			this->InputMoney->Name = L"InputMoney";
			this->InputMoney->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->InputMoney->Size = System::Drawing::Size(232, 20);
			this->InputMoney->TabIndex = 1;
			this->InputMoney->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Location = System::Drawing::Point(96, 151);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(96, 13);
			this->label3->TabIndex = 5;
			this->label3->Text = L"���� (� �������) ";
			// 
			// label4
			// 
			this->label4->AutoSize = true;
			this->label4->Location = System::Drawing::Point(80, 227);
			this->label4->Name = L"label4";
			this->label4->Size = System::Drawing::Size(126, 13);
			this->label4->TabIndex = 6;
			this->label4->Text = L"������������� ������";
			// 
			// income
			// 
			this->income->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->income->FormattingEnabled = true;
			this->income->Items->AddRange(gcnew cli::array< System::Object^  >(4) { L"�� ��������", L"�� ����� �����", L"2-����", L"3-����" });
			this->income->Location = System::Drawing::Point(31, 248);
			this->income->Name = L"income";
			this->income->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->income->Size = System::Drawing::Size(231, 21);
			this->income->TabIndex = 7;
			// 
			// guarantee
			// 
			this->guarantee->DropDownStyle = System::Windows::Forms::ComboBoxStyle::DropDownList;
			this->guarantee->FormattingEnabled = true;
			this->guarantee->Items->AddRange(gcnew cli::array< System::Object^  >(4) { L"�� ��������", L"����������", L"����� ����", L"����� ������������" });
			this->guarantee->Location = System::Drawing::Point(31, 320);
			this->guarantee->Name = L"guarantee";
			this->guarantee->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->guarantee->Size = System::Drawing::Size(231, 21);
			this->guarantee->TabIndex = 9;
			// 
			// label5
			// 
			this->label5->AutoSize = true;
			this->label5->Location = System::Drawing::Point(109, 299);
			this->label5->Name = L"label5";
			this->label5->Size = System::Drawing::Size(74, 13);
			this->label5->TabIndex = 8;
			this->label5->Text = L"�����������";
			// 
			// InputPeriod
			// 
			this->InputPeriod->Location = System::Drawing::Point(31, 170);
			this->InputPeriod->Name = L"InputPeriod";
			this->InputPeriod->RightToLeft = System::Windows::Forms::RightToLeft::No;
			this->InputPeriod->Size = System::Drawing::Size(231, 20);
			this->InputPeriod->TabIndex = 11;
			this->InputPeriod->TextAlign = System::Windows::Forms::HorizontalAlignment::Center;
			// 
			// calculate
			// 
			this->calculate->Location = System::Drawing::Point(31, 377);
			this->calculate->Name = L"calculate";
			this->calculate->Size = System::Drawing::Size(231, 29);
			this->calculate->TabIndex = 12;
			this->calculate->Text = L"����������";
			this->calculate->UseVisualStyleBackColor = true;
			this->calculate->Click += gcnew System::EventHandler(this, &MyForm::button1_Click_1);
			// 
			// Output
			// 
			this->Output->AutoSize = true;
			this->Output->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 10, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->Output->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(192)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->Output->Location = System::Drawing::Point(377, 42);
			this->Output->Name = L"Output";
			this->Output->Size = System::Drawing::Size(0, 17);
			this->Output->TabIndex = 13;
			// 
			// webBrowser1
			// 
			this->webBrowser1->Location = System::Drawing::Point(365, 60);
			this->webBrowser1->MinimumSize = System::Drawing::Size(20, 20);
			this->webBrowser1->Name = L"webBrowser1";
			this->webBrowser1->ScrollBarsEnabled = false;
			this->webBrowser1->Size = System::Drawing::Size(300, 291);
			this->webBrowser1->TabIndex = 14;
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(328, 180);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(31, 23);
			this->button1->TabIndex = 15;
			this->button1->Text = L">";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm::button3_Click);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(671, 180);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(31, 23);
			this->button2->TabIndex = 16;
			this->button2->Text = L"<";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &MyForm::button2_Click);
			// 
			// button3
			// 
			this->button3->Location = System::Drawing::Point(328, 378);
			this->button3->Name = L"button3";
			this->button3->Size = System::Drawing::Size(147, 28);
			this->button3->TabIndex = 17;
			this->button3->Text = L"������ ������ � �������";
			this->button3->UseVisualStyleBackColor = true;
			this->button3->Click += gcnew System::EventHandler(this, &MyForm::button3_Click_1);
			// 
			// pictureBox2
			// 
			this->pictureBox2->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox2.Image")));
			this->pictureBox2->InitialImage = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox2.InitialImage")));
			this->pictureBox2->Location = System::Drawing::Point(15, 12);
			this->pictureBox2->Name = L"pictureBox2";
			this->pictureBox2->Size = System::Drawing::Size(263, 63);
			this->pictureBox2->TabIndex = 19;
			this->pictureBox2->TabStop = false;
			this->pictureBox2->WaitOnLoad = true;
			// 
			// button4
			// 
			this->button4->Location = System::Drawing::Point(490, 377);
			this->button4->Name = L"button4";
			this->button4->Size = System::Drawing::Size(221, 28);
			this->button4->TabIndex = 20;
			this->button4->Text = L"����������� ���������� � �������";
			this->button4->UseVisualStyleBackColor = true;
			this->button4->Click += gcnew System::EventHandler(this, &MyForm::button4_Click);
			// 
			// webBrowser2
			// 
			this->webBrowser2->Location = System::Drawing::Point(710, 12);
			this->webBrowser2->MinimumSize = System::Drawing::Size(20, 20);
			this->webBrowser2->Name = L"webBrowser2";
			this->webBrowser2->Size = System::Drawing::Size(20, 20);
			this->webBrowser2->TabIndex = 21;
			this->webBrowser2->Visible = false;
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::SystemColors::ControlLightLight;
			this->ClientSize = System::Drawing::Size(742, 437);
			this->Controls->Add(this->webBrowser2);
			this->Controls->Add(this->button4);
			this->Controls->Add(this->pictureBox2);
			this->Controls->Add(this->button3);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->webBrowser1);
			this->Controls->Add(this->Output);
			this->Controls->Add(this->calculate);
			this->Controls->Add(this->InputPeriod);
			this->Controls->Add(this->guarantee);
			this->Controls->Add(this->label5);
			this->Controls->Add(this->income);
			this->Controls->Add(this->label4);
			this->Controls->Add(this->label3);
			this->Controls->Add(this->InputMoney);
			this->Controls->Add(this->label1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
//			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->MaximizeBox = false;
			this->MinimizeBox = false;
			this->Name = L"MyForm";
			this->RightToLeft = System::Windows::Forms::RightToLeft::Yes;
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterParent;
			this->Text = L"JustCredit";
			this->Load += gcnew System::EventHandler(this, &MyForm::MyForm_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

	private: System::Void button1_Click_1(System::Object^  sender, System::EventArgs^  e) {
		num = 0;
		credit.AutoBail = 0;
		credit.EstateBail = 0;
		credit.CertificateFromBank = 0;
		credit.Guaranator = 0;
		credit.Period = 0;
		credit.Sum = 0;
		credit.ThreeNDFL = 0;
		credit.TwoNDFL = 0;

		String ^str = (InputMoney->Text);
		if (CheckTextBox(str))
		{
			credit.Sum = System::Convert::ToInt32(str);
		}
		else
			credit.Sum = 0;

		bool uncorrect;
		uncorrect = false;

		if ((credit.Sum < 10000) || (credit.Sum>15000000))
		{
			MessageBox::Show("�� ����� ������������ ������.");
			uncorrect = true;
		}

		str = InputPeriod->Text;
		if (CheckTextBox(str))
		{
			credit.Period = System::Convert::ToInt32(str);
		}
		else
			credit.Period = 1;

		if ((credit.Period<1) || (credit.Period>240))
		{
			MessageBox::Show("�� ����� ������������ ������.");
			uncorrect = true;
		}
		if (income->SelectedItem == "�� ��������")
		{
			credit.CertificateFromBank = 2;
			credit.TwoNDFL = 2;
			credit.ThreeNDFL = 2;
		}
		if (income->SelectedItem == "2-����")
		{
			credit.TwoNDFL = 1;
		}
			
		if (income->SelectedItem == "�� ����� �����")
		{
			credit.CertificateFromBank = 1;
		}

		if (income->SelectedItem == "3-����")
		{
			credit.ThreeNDFL = 1;
		}

		if (guarantee->SelectedItem == "�� ��������")
		{
			credit.AutoBail = 2;
			credit.EstateBail = 2;
			credit.Guaranator = 2;
		}
		if (guarantee->SelectedItem == "����� ������������")
		{
			credit.EstateBail = 1;
		}

		if (guarantee->SelectedItem == "����� ����")
		{
			credit.AutoBail = 1;
			uncorrect = true;
		}

		if (guarantee->SelectedItem == "����������")
		{
			credit.Guaranator = 1;
		}
		if (uncorrect == false)
		{
			button1->Enabled = true;
			button2->Enabled = true;
			button3->Enabled = true;
			banks.DataCredit.clear();
			banks.CorrectDataCredit.clear();
			banks.FileToVector(credit);
			banks.Write(0);
			String^ path = Directory::GetCurrentDirectory();
			path = path + "/" + "index.html";
			webBrowser1->Navigate(path);
			Output->Text = "	������ �������";
		}
		if (uncorrect == true)
		{
			String^ path = Directory::GetCurrentDirectory();
			webBrowser1->Navigate(path + "/" + "uncorrect.html");
			button1->Enabled = false;
			button2->Enabled = false;
			button3->Enabled = false;
		}
		
	}
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
	if (num < banks.CorrectDataCredit.size()-1)
	{
		num++;
		WriteHtml(num);
		webBrowser1->Refresh();
	}
	Output->Text = "";
	if (num == 0)
	{
		Output->Text = "	������ �������";
	}
}
private: System::Void button3_Click(System::Object^  sender, System::EventArgs^  e) {
	if (num > 0)
	{
		num--;
		WriteHtml(num);
		webBrowser1->Refresh();
	}
	Output->Text = "";
	if (num == 0)
	{
		Output->Text = "	������ �������";
	}
}
private: System::Void MyForm_Load(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void button3_Click_1(System::Object^  sender, System::EventArgs^  e) {
	String^ path = Directory::GetCurrentDirectory();
	System::Diagnostics::Process::Start("http://justcredit.esy.es/" + banks.CorrectDataCredit[num].number + ".htm");
}
private: System::Void button4_Click(System::Object^  sender, System::EventArgs^  e) {
	String^ path = Directory::GetCurrentDirectory();
	webBrowser2->Navigate("http://justcredit.esy.es/" + banks.CorrectDataCredit[num].number + ".htm");
	webBrowser2->Print();
}
};
}
