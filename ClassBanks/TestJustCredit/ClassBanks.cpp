#include "stdafx.h"
#include "ClassHeader.h"
class WorkWithBanks;

void WorkWithBanks::CalculationPayment(int Sum, int Period)
{
	for (unsigned int i = 0; i < DataCredit.size(); i++)
	{
		DataCredit[i].Payment = ceil((AnnuityPayment(Sum, Period, DataCredit[i].Rate)) * 100 + 0.5) / 100;
		DataCredit[i].Overpayment = ceil((AllOvetpayment(Sum, Period, DataCredit[i].Payment)) * 100 + 0.5) / 100;
	}
}

double WorkWithBanks::AnnuityPayment(int Sum, int Period, double Rate)
{
	double P = 1 / 12.0 * Rate / 100.0;
	return (Sum*(P + (P / (pow((1 + P), Period) - 1))));
}

double WorkWithBanks::AllOvetpayment(int Sum, int Period, double MonthlyPayment)
{
	return MonthlyPayment*Period - Sum;
}

wstring WorkWithBanks::CopyStr(wstring _str, int a, int b)
{
	wstring res;
	for (int i = a; i < b; i++)
	{
		res = res + _str[i];
	}
	return res;
}

void WorkWithBanks::OpenFile()
{
	wifstream fin;
	fin.open("CredirBanks.txt");
	if (!fin.is_open())
	{
		wcout << L"File not found!";
		_tsystem(L"Pause");
		exit(1);
	}
	getline(fin, str, L'\0');
	fin.close();
}

void WorkWithBanks::DeleteString()
{
	str.erase(str.find(L'<'), str.find(L'>') - str.find(L'<') + 1);
}

void WorkWithBanks::CopyString(wstring &_str)
{
	_str = CopyStr(str, str.find(L'<') + 1, str.find(L'>'));
	DeleteString();
}

void WorkWithBanks::CopyRealNumber(double &_number)
{
	_number = stof(CopyStr(str, str.find(L'<') + 1, str.find(L'>')));
	DeleteString();
}

void WorkWithBanks::CopyIntNumber(int &_number)
{
	_number = stoi(CopyStr(str, str.find(L'<') + 1, str.find(L'>')));
	DeleteString();
}

 void WorkWithBanks::Write(int i)
{
	wstring item;
	wifstream fin;
	fin.open("bd.html");
	getline(fin, str, L'\0');
	fin.close();
	wofstream fout;
	fout.open("index.html");
	str.insert(str.find(L"@"), to_wstring(i+1));
	str.erase(str.find(L"@"), 1);
	str.insert(str.find(L"@"), to_wstring(CorrectDataCredit.size()));
	str.erase(str.find(L"@"), 1);
	str.insert(str.find(L"@"), CorrectDataCredit[i].NameBank);
	str.erase(str.find(L"@"), 1);
	str.insert(str.find(L"@"), CorrectDataCredit[i].NameCredit);
	str.erase(str.find(L"@"), 1);
	item = Format(to_wstring(CorrectDataCredit[i].Rate));
	str.insert(str.find(L"@"), item);
	str.erase(str.find(L"@"), 1);
	item = Format(to_wstring(CorrectDataCredit[i].Payment));
	str.insert(str.find(L"@"), item);
	str.erase(str.find(L"@"), 1);
	item = Format(to_wstring(CorrectDataCredit[i].Overpayment));
	str.insert(str.find(L"@"), item);
	str.erase(str.find(L"@"), 1);

	fout << str;
	fout.close();
}

int wmain()
{
	return 0;
}

wstring WorkWithBanks::Format(wstring _str)
{
	int i;
	i = 0;
	while (_str[i] != '.')
	{
		i++;
	}
	for (int j = i - 3; j > 0; j-=3)
	{
		_str.insert(j, L" ");
	}
	i = _str.size();
	do
	{
		_str.erase(i, 1);
		if (_str[i - 1] == '.')
		{
			_str.erase(i - 1, 1);
		}
		--i;
	} while (_str[i] == '0');
	
	return _str;
}

void WorkWithBanks::RegularizeCorrectDataCredit()
{
	for (unsigned int i = 0; i < CorrectDataCredit.size()-1; i++)
	{
		for (unsigned int j = i + 1; j < CorrectDataCredit.size(); j++)
		{
			if (CorrectDataCredit[i].Payment > CorrectDataCredit[j].Payment)
			{
				OneCredit inf = CorrectDataCredit[j];
				CorrectDataCredit[j] = CorrectDataCredit[i];
				CorrectDataCredit[i] = inf;
			}
		}
	}
}

bool WorkWithBanks::CheckFlag(int data, int correctdata, bool flag)
{
	if (correctdata == 1)
	{
		if (correctdata == data && flag == true)
		{
			flag = true;
		}
		else flag = false;
	}
	return flag;
}

void WorkWithBanks::CreateCorrectDataCredit(InputData data)
{
	for (unsigned int i = 0; i < DataCredit.size(); ++i)
	{
		bool FLAG = true;

		if (data.Sum >= DataCredit[i].MinSum && data.Sum <= DataCredit[i].MaxSum && FLAG == true)
		{
			FLAG = true;
		}
		else FLAG = false;

		if (data.Period >= DataCredit[i].MinPeriod && data.Period <= DataCredit[i].MaxPeriod && FLAG == true)
		{
			FLAG = true;
		}
		else FLAG = false;

		FLAG = CheckFlag(DataCredit[i].CertificateFromBank, data.CertificateFromBank, FLAG);
		FLAG = CheckFlag(DataCredit[i].TwoNDFL, data.TwoNDFL, FLAG);
		FLAG = CheckFlag(DataCredit[i].ThreeNDFL, data.ThreeNDFL, FLAG);
		FLAG = CheckFlag(DataCredit[i].Guaranator, data.Guaranator, FLAG);
		FLAG = CheckFlag(DataCredit[i].EstateBail, data.EstateBail, FLAG);
		FLAG = CheckFlag(DataCredit[i].AutoBail, data.AutoBail, FLAG);

		if (FLAG == true)
		{
			CorrectDataCredit.push_back(DataCredit[i]);
		}
	}
	RegularizeCorrectDataCredit();
}

void WorkWithBanks::FileToVector(InputData data)
{
	OneCredit banks;
	OpenFile();
	while (str.find(L'<') - str.find(L'>') != 0)
	{
		CopyString(banks.NameBank);
		CopyString(banks.NameCredit);
		CopyRealNumber(banks.Rate);
		CopyIntNumber(banks.MinSum);
		CopyIntNumber(banks.MaxSum);
		CopyIntNumber(banks.MinPeriod);
		CopyIntNumber(banks.MaxPeriod);
		CopyIntNumber(banks.CertificateFromBank);
		CopyIntNumber(banks.TwoNDFL);
		CopyIntNumber(banks.ThreeNDFL);
		CopyIntNumber(banks.Guaranator);
		CopyIntNumber(banks.EstateBail);
		CopyIntNumber(banks.AutoBail);
		CopyRealNumber(banks.Payment);
		CopyRealNumber(banks.Overpayment);
		CopyIntNumber(banks.number);

		DataCredit.push_back(banks);
	}
	CalculationPayment(data.Sum, data.Period);
	CreateCorrectDataCredit(data);
}