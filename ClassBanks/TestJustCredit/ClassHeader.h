#pragma once

#include "stdafx.h"
#include <math.h>
#include <vector>
#include <fstream>
#include <string>
#include <iostream>

using namespace std;

struct InputData
{
	int Sum;
	int Period;
	int CertificateFromBank;
	int TwoNDFL;
	int ThreeNDFL;
	int Guaranator;
	int EstateBail;
	int AutoBail;
};

class WorkWithBanks
{
private:
	struct OneCredit
	{
		wstring NameBank;
		wstring NameCredit;
		double Rate;
		int MinSum;
		int MaxSum;
		int MinPeriod;
		int MaxPeriod;
		int CertificateFromBank;
		int TwoNDFL;
		int ThreeNDFL;
		int Guaranator;
		int EstateBail;
		int AutoBail;
		double Payment;
		double Overpayment;
		int number;
	};

	wstring str;

	double AnnuityPayment(int Sum, int Period, double Rate);
	double AllOvetpayment(int Sum, int Period, double MonthlyPayment);
	void CalculationPayment(int Sum, int Period);
	wstring CopyStr(wstring _str, int a, int b);
	void OpenFile();
	void DeleteString();
	void CopyString(wstring &_str);
	void CopyRealNumber(double &_number);
	void CopyIntNumber(int &_number);
	void RegularizeCorrectDataCredit();
	bool CheckFlag(int data, int correctdata, bool flag);
public:
	vector<OneCredit> DataCredit;
	vector<OneCredit> CorrectDataCredit;
	void FileToVector(InputData data);
	void Write(int i);
	void CreateCorrectDataCredit(InputData data);
	wstring Format(wstring _str);
};