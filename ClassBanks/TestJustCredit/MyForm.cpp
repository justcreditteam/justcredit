#include "MyForm.h"
#include "ClassHeader.h"
using namespace System;
using namespace System::Windows::Forms;


[STAThreadAttribute]
void Main() {
	Application::EnableVisualStyles();
	Application::SetCompatibleTextRenderingDefault(false);
	TestJustCredit::MyForm form;
	Application::Run(%form);
}